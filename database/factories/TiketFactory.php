<?php


use Faker\Generator as Faker;
use App\Model\Category\Category;

$factory->define(App\Model\Tiket\Tiket::class, function (Faker $faker) {
    $word= $faker->word;
    return [
        'nama_tiket'=>$word,
        'harga_tiket'=>rand(1,1000),
        'jumlah_tiket'=>rand(1,30),
        'id_kategori'=>function(){
          return Category::all()->random();
        },
        'jenis_tiket'=>$word
    ];
});
